@echo off

rem Set paths:
rem --------------------------------------------
set BatchPath=C:\fip\local\scripts\PortalReports
set TempPath=C:\fip\local\scripts\PortalReports\temp
set DestPath=\\Filprint\AlleNTB\PORTAL\Reports 

rem Set current directory to Report folder
rem --------------------------------------------
C:
cd %TempPath%

rem Main program:
rem --------------------------------------------

date /T >%BatchPath%\ReportTransfere.out
time /T >>%BatchPath%\ReportTransfere.out
echo --------------------------------->>%BatchPath%\ReportTransfere.out
echo Gets all reportfiles from portal >>%BatchPath%\ReportTransfere.out

ftp -s:%BatchPath%\ReportTransfere.ftp 193.75.33.39 >>%BatchPath%\ReportTransfere.out

rem Check if transfere was OK
find "226 Transfer complete." %BatchPath%\ReportTransfere.out>nul

if errorlevel 1 goto error
	echo --------------------------------->>%BatchPath%\ReportTransfere.out
	echo Copying files to %DestPath%>>%BatchPath%\ReportTransfere.out

	xcopy *.* %DestPath% /Y>>%BatchPath%\ReportTransfere.out

if errorlevel 1 goto error
	echo --------------------------------->>%BatchPath%\ReportTransfere.out
	echo Deleting files:>>%BatchPath%\ReportTransfere.out

	del %TempPath%\*.* /Q>>%BatchPath%\ReportTransfere.out
	ftp -s:%BatchPath%\ReportDelete.ftp 193.75.33.39>>%BatchPath%\ReportTransfere.out

	echo ------------->>%BatchPath%\ReportTransfere.out
	echo Transfere OK!>>%BatchPath%\ReportTransfere.out

goto end

:error
	echo ----------------->>%BatchPath%\ReportTransfere.out
	echo Transfere FAILED!>>%BatchPath%\ReportTransfere.out

:end
